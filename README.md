# ARE003 Electronics

This repository contains information on all PCBs used for the Aviathil Flight Guidance Panel CJ4 project (ARE003).

## ARE003-B01 Controller

Main controller for ARE003. General features:

* On-board microcontroller (Teensy 4.0)
* USB peripheral interface
* 7x on-board rotary encoders (6x with switch, 1x without switch)
* 1x on-board slide switch for YD/AP DISC
* Supports 15x pushbutton switches with LEDs (off-board, via connectors)
* 7-segment LED display for autopilot parameters
* Designed to mount directly behind front panel

**For flight simulation use only.**

### Changelog

| Rev | Date       | Changes                             | Author |
| --: | ---------- | ----------------------------------- | ------ |
|   1 | 2024-02-19 | Initial revision. NEVER MANUFACTURED. | L.CHAN |
|   2 | 2024-02-22 | Fix overlap in indicator LEDs connected to U2. Multiple buttons/encoder switches and indicator LEDs connected to U2 were shuffled around. | L.CHAN |

### Renders

![Render of ARE003-B01, top side](img/ARE003-B01-controller-top.png)

![Render of ARE003-B01, bottom side](img/ARE003-B01-controller-bot.png)

### External assembly

* USB connector
    * This device is designed to connect a back panel USB connector to the on-board Teensy's USB connector via cable. On the Teensy side, the cable can either mate to the Teensy's USB micro-B connector, or can solder wires directly to the USB pads on the bottom of the PCB.
    * If using external USB-C connector, the connector must include power-setting resistors or USB-PD negotiation in order to enable at least 1A current draw at 5V.
* Power: This device is powered by the external USB connection. The upstream device must be able to supply at least 1A @ +5V.
* Mounting
	* In order to avoid PCB flexing and fatiguing or failure of solder joints and electronic components, ensure that the PCB is properly affixed to the front panel at all mount points.
	* Encoder nuts are MANDATORY mount points. In particular, DO NOT use encoders without affixing them to the front panel using encoder nuts. Do NOT install encoders without bushings for front-panel parts. (The VS encoder does not require a bushing.)

### Peripheral configuration

* Encoders: If hardware RC filters are NOT installed, then all encoder A/B pins must be configured as input with pull-up enabled. If hardware RC filters ARE installed, then pull-ups must be DISABLED.
* AS1115
  * Enable self-addressing prior to any other communication with the AS1115 devices.
  * Configure to scan all 8 digits.
  * Configure the intensity control register.
  * Configure to scan both keyswitch rows.
  * Address 0x0: configure HEX decode on digits 0-6, no decode on digit 7.
  * Address 0x3: configure HEX decode on digits 0-5, no decode on digits 6-7.
  * Other start-up configurations may be needed. Refer to the AS1115 manual.

### MCU I/O Table

| Pin# | Peripheral      | Function         | Description     |
| ---- | --------------- | ---------------- | --------------- |
| 0    | GPIO/UART1/CAN2 | RX1              | Extra GPIO      |
| 1    | GPIO/UART1/CAN2 | TX1              | Extra GPIO      |
| 2    | GPIO            | DIN, PULLUP      | ENC CRS1 B      |
| 3    | GPIO            | DIN, PULLUP      | ENC CRS1 A      |
| 4    | GPIO            | DIN, PULLUP      | ENC CRS2 B      |
| 5    | GPIO            | DIN, PULLUP      | ENC CRS2 A      |
| 6    | GPIO            | DIN, PULLUP      | ENC BARO B      |
| 7    | GPIO            | DIN, PULLUP      | ENC BARO A      |
| 8    | GPIO            | DIN, PULLUP, INT | IRQ             |
| 9    | GPIO            | DIN, PULLUP      | ENC VS B        |
| 10   | GPIO            | DIN, PULLUP      | ENC VS A        |
| 11   | GPIO/SPI0       | MOSI0            | Extra GPIO      |
| 12   | GPIO/SPI0       | MISO0            | Extra GPIO      |
| 13   | GPIO/SPI0       | SCK0             | Extra GPIO      |
| 14   | GPIO/UART3/ADC  | TX3/A0/PWM       | Extra GPIO      |
| 15   | GPIO/UART3/ADC  | TX3/A1/PWM       | Extra GPIO      |
| 16   | GPIO            | DIN, PULLUP      | ENC SPD B       |
| 17   | GPIO            | DIN, PULLUP      | ENC SPD A       |
| 18   | I2C0            | SDA0             | I2C bus, 400kHz |
| 19   | I2C0            | SCL0             | I2C bus, 400kHz |
| 20   | GPIO            | DIN, PULLUP      | ENC HDG B       |
| 21   | GPIO            | DIN, PULLUP      | ENC HDG A       |
| 22   | GPIO, CAN1, ADC | DIN, PULLUP      | ENC ALT B       |
| 23   | GPIO, CAN1, ADC | DIN, PULLUP      | ENC ALT A       |

### AS1115 LED Table

| I2Caddr | Digit | Description              |
| ------- | ----- | ------------------------ |
| 0x0     | 0     | VS (BARO/CRS2/CRS1) 1000 |
| 0x0     | 1     | VS (BARO/CRS2/CRS1) 100  |
| 0x00    | 2     | VS (BARO/CRS2/CRS1) 10   |
| 0x00    | 3     | VS (BARO/CRS2/CRS1) 1    |
| 0x00    | 4     | SPEED 100                |
| 0x00    | 5     | SPEED 10                 |
| 0x00    | 6     | SPEED 1                  |
| 0x00    | 7A    | FD indicator             |
| 0x00    | 7B    | VS indicator             |
| 0x00    | 7C    | VNAV indicator           |
| 0x00    | 7D    | FLC indicator            |
| 0x00    | 7E    | ATHR indicator           |
| 0x00    | 7F    | IAS indicator            |
| 0x00    | 7G    | MACH indicator           |
| 0x00    | 7DP   | MAN indicator            |
| 0x03    | 0     | HDG 100                  |
| 0x03    | 1     | HDG 10                   |
| 0x03    | 2     | HDG 1                    |
| 0x03    | 3     | ALT 100                  |
| 0x03    | 4     | ALT 10                   |
| 0x03    | 5     | ALT 1                    |
| 0x03    | 6A    | ALT indicator            |
| 0x03    | 6B    | N.C.                     |
| 0x03    | 6C    | YD indicator             |
| 0x03    | 6D    | AP indicator             |
| 0x03    | 6E    | APXFR indicator          |
| 0x03    | 7A    | NAV indicator            |
| 0x03    | 7B    | 1/2 BANK indicator       |
| 0x03    | 7C    | HDG indicator            |
| 0x03    | 7D    | N.C.                     |
| 0x03    | 7E    | APPR indicator           |
| 0x03    | 7DP   | B/C indicator            |

### AS1115 Keyscan Table

| I2C addr | reg name | reg addr | seg | bit | description        |
| -------- | -------- | -------- | --- | --- | ------------------ |
| 0x00     | KEYB     | 0x1D     | DP  | 7   | MAN (ATHR mode) SW |
| 0x00     | KEYB     | 0x1D     | A   | 6   | FD SW              |
| 0x00     | KEYB     | 0x1D     | B   | 5   | VS SW              |
| 0x00     | KEYB     | 0x1D     | C   | 4   | VNAV SW            |
| 0x00     | KEYB     | 0x1D     | D   | 3   | FLC SW             |
| 0x00     | KEYB     | 0x1D     | E   | 2   | ATHR SW            |
| 0x00     | KEYB     | 0x1D     | F   | 1   | n/a                |
| 0x00     | KEYB     | 0x1D     | G   | 0   | n/a                |
| 0x00     | KEYA     | 0x1C     | DP  | 7   | n/a                |
| 0x00     | KEYA     | 0x1C     | A   | 6   | CRS1 SW            |
| 0x00     | KEYA     | 0x1C     | B   | 5   | CRS2 SW            |
| 0x00     | KEYA     | 0x1C     | C   | 4   | BARO SW            |
| 0x00     | KEYA     | 0x1C     | D   | 3   | SPD SW             |
| 0x00     | KEYA     | 0x1C     | E   | 2   | n/a                |
| 0x00     | KEYA     | 0x1C     | F   | 1   | n/a                |
| 0x00     | KEYA     | 0x1C     | G   | 0   | n/a                |
| 0x03     | KEYB     | 0x1D     | DP  | 7   | B/C SW             |
| 0x03     | KEYB     | 0x1D     | A   | 6   | NAV SW             |
| 0x03     | KEYB     | 0x1D     | B   | 5   | 1/2 BANK SW        |
| 0x03     | KEYB     | 0x1D     | C   | 4   | HDG SW             |
| 0x03     | KEYB     | 0x1D     | D   | 3   | HDG ENC SW         |
| 0x03     | KEYB     | 0x1D     | E   | 2   | APPR SW            |
| 0x03     | KEYB     | 0x1D     | F   | 1   | n/a                |
| 0x03     | KEYB     | 0x1D     | G   | 0   | n/a                |
| 0x03     | KEYA     | 0x1C     | DP  | 7   | YD/AP DISC SW      |
| 0x03     | KEYA     | 0x1C     | A   | 6   | ALT SW             |
| 0x03     | KEYA     | 0x1C     | B   | 5   | ALT ENC SW         |
| 0x03     | KEYA     | 0x1C     | C   | 4   | YD SW              |
| 0x03     | KEYA     | 0x1C     | D   | 3   | AP SW              |
| 0x03     | KEYA     | 0x1C     | E   | 2   | AP XFER SW         |
| 0x03     | KEYA     | 0x1C     | F   | 1   | n/a                |
| 0x03     | KEYA     | 0x1C     | G   | 0   | n/a                |

### Output artefacts

For schematics, fabrication files, centroid files, BOM, and other artefacts, see [Releases](-/releases).
